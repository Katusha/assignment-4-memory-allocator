//
// Created by HP on 11.12.2022.
//

#define _DEFAULT_SOURCE

#include "own_tests.h"

#define MALLOC_SIZE 1024

int free_heap(void *heap, size_t size) {
    return munmap(heap, size_from_capacity((block_capacity) {size}).bytes);
}

bool default_success_allocation() {

    printf("Test1: START");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        printf("Test1: FAIL");
        return false;
    }

    void *malloc_result = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);

    if (malloc_result == NULL) {
        printf("Test1: FAIL");
        return false;
    }

    _free(malloc_result);
    free_heap(heap, REGION_MIN_SIZE);

    printf("Test1: SUCCESS");
    return true;

}

bool free_one_block_allocation() {

    printf("Test2: START");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        printf("Test2: FAIL");
        return false;
    }

    void *malloc_result_0 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_1 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_2 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL || malloc_result_1 == NULL || malloc_result_2 == NULL) {
        printf("Test2: FAIL");
        return false;
    }

    _free(malloc_result_1);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL || malloc_result_2 == NULL) {
        printf("Test2: FAIL");
        return false;
    }

    _free(malloc_result_0);
    _free(malloc_result_2);
    free_heap(heap, REGION_MIN_SIZE);

    printf("Test2: SUCCESS");
    return true;

}

bool free_two_blocks_allocation() {

    printf("Test3: START");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        printf("Test3: FAIL");
        return false;
    }

    void *malloc_result_0 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_1 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);
    void *malloc_result_2 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL || malloc_result_1 == NULL || malloc_result_2 == NULL) {
        printf("Test3: FAIL");
        return false;
    }

    _free(malloc_result_1);
    debug_heap(stdout, heap);
    _free(malloc_result_2);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL) {
        printf("Test3: FAIL");
        return false;
    }

    _free(malloc_result_0);
    free_heap(heap, REGION_MIN_SIZE);

    printf("Test3: SUCCESS");
    return true;

}

bool grow_heap_after_is_free() {

    printf("Test4: START");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        printf("Test4: FAIL");
        return false;
    }

    void *malloc_result = _malloc(MALLOC_SIZE * 10);
    debug_heap(stdout, heap);

    if (malloc_result == NULL) {
        printf("Test4: FAIL");
        return false;
    }

    struct block_header *header = heap;

    if (header->capacity.bytes <= REGION_MIN_SIZE) {
        printf("Test4: FAIL");
        return false;
    }

    _free(malloc_result);
    free_heap(heap, REGION_MIN_SIZE);

    printf("Test4: SUCCESS");
    return true;

}

bool grow_heap_after_is_not_free() {

    printf("Test5: START");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    if (heap == NULL) {
        printf("Test5: FAIL");
        return false;
    }

    void *malloc_result_0 = _malloc(MALLOC_SIZE);
    debug_heap(stdout, heap);

    if (malloc_result_0 == NULL) {
        printf("Test5: FAIL");
        return false;
    }

    struct block_header *header = (struct block_header*) (malloc_result_0 - offsetof(struct block_header, contents));

    (void) mmap((void *) header->contents + header->capacity.bytes, 2048, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    debug_heap(stdout, heap);

    void *malloc_result_1 = _malloc(REGION_MIN_SIZE * 2);
    debug_heap(stdout, heap);

    if (malloc_result_1 == NULL) {
        printf("Test5: FAIL");
        return false;
    }

    _free(malloc_result_0);
    _free(malloc_result_1);
    free_heap(heap, REGION_MIN_SIZE);

    printf("Test5: SUCCESS");
    return true;

}


