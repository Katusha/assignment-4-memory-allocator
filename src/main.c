//
// Created by HP on 11.12.2022.
//

#include "own_tests.h"

#define test_count 5

int main() {
    int passed = 0;
    passed += default_success_allocation();
    passed += free_one_block_allocation();
    passed += free_two_blocks_allocation();
    passed += grow_heap_after_is_free();
    passed += grow_heap_after_is_not_free();

    if (passed == test_count) {
        printf("All tests passed!");
    } else {
        printf("Passed %d out of %d tests", passed, test_count);
    }
}
